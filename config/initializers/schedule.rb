require 'rufus-scheduler'

scheduler2 = Rufus::Scheduler.new
# scheduler.every '15m' do
#   Rails.logger.info "This 15m"
#   app = FbGraph::Application.new(300450546746887, :secret => '71e6e5ab681e76090a2da7065905bb7a')
#   @token = app.get_access_token
#   @page = FbGraph::Page.fetch('dressxmeup', :access_token => @token)

#   items = Facebook.find_by_name("Items")
#   value = @page.albums({limit: 999}).map{ |a| {name: a.name, identifier: a.identifier, description: a.description, first_photo: a.photos.first.images[2].source} if a.name.match('Profile Pictures').blank? }.compact!
#   items.value = value
#   puts "Done"if items.save
# end

scheduler2.every '15m' do
  Rails.logger.info "--- START UPDATE ITEMS ---"

  app = FbGraph::Application.new(300450546746887, :secret => '71e6e5ab681e76090a2da7065905bb7a')
  Rails.logger.info "--- GET APP ---" if app.present?

  @token = app.get_access_token
  Rails.logger.info "--- GET TOKEN ---" if @token.present?

  @page = FbGraph::Page.fetch('dressxmeup', :access_token => @token)
  Rails.logger.info "--- GET PAGE ---" if @page.present?  

  items = Facebook.find_by_name("Items")
  Rails.logger.info "--- GET ITEMS ---" if @items.present?

  value = @page.albums({limit: 999}).map{ |a| {name: a.name, identifier: a.identifier, description: a.description, first_photo: a.photos.first.images[2].source} if a.name.match('Profile Pictures').blank? }.compact!
  Rails.logger.info "--- GET NEW VALUE ---" if value.present?

  items.value = value
  Rails.logger.info "--- SAVED NEW VALUE ---" if items.save
end

