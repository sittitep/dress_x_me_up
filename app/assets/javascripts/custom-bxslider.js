$(document).ready(function(){
  $('.bxslider').bxSlider({
    pagerCustom: '#bx-pager'
  });
});

$( window ).load(function() {
    if($(".bx-viewport").length > 0){
      var highest = $(".bx-viewport").height()
      $(".bx-viewport li").each(function(){
        if ($(this).height() < highest){
         $(this).css("margin-top",(highest-$(this).height())/2 + "px") 
        }
      });
    }
});