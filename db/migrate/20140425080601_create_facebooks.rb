class CreateFacebooks < ActiveRecord::Migration
  def change
    create_table :facebooks do |t|
      t.string :name
      t.text :value

      t.timestamps
    end
  end
end
